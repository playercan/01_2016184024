#include "stdafx.h"
#include "GSEGame.h"


GSEGame::GSEGame() 
{
	//renderer initialize
	m_renderer = new Renderer(GSE_WINDOWSIZE_X, GSE_WINDOWSIZE_Y);
	m_Sound = new Sound();
	for (int i = 0; i < GSE_MAX_OBJECTS; i++) 
	{
		m_objects[i] = NULL;
	}

	//hero anim
	m_HeroIdleTexture =  m_renderer->GenPngTexture("idle.png");
	m_HeroAttackTexture = m_renderer->GenPngTexture("Hit.png");
	m_HeroAttackLeftTexture = m_renderer->GenPngTexture("HitLeft.png");
	m_HeroRunTexture = m_renderer->GenPngTexture("Run.png");
	m_HeroRunLeftTexture = m_renderer->GenPngTexture("RunLeft.png");
	m_HeroFallTextrue = m_renderer->GenPngTexture("fall.png");


	//hero magic anim
	m_HeroMagicTextrue = m_renderer->GenPngTexture("magic.png");

	//Brick Textrue
	m_BrickTexture = m_renderer->GenPngTexture("Bricks.png"); 
	m_WallTexture = m_renderer->GenPngTexture("Wall.png");

	//bg Texture
	m_BGTexture = m_renderer->GenPngTexture("sky.png");
	m_BGTexture2 = m_renderer->GenPngTexture("skyShort.png");
	m_BGTexture3 = m_renderer->GenPngTexture("dawnSky.png");
	m_BGTexture4 = m_renderer->GenPngTexture("dawnSky1.png");

	m_StartBGTexture = m_renderer->GenPngTexture("StartPage.png");
	m_GameOverBGTextrue = m_renderer->GenPngTexture("GameOverPage.png");
	m_ClearBGTexture = m_renderer->GenPngTexture("ClearPage.png");


	//particle Textrue
	m_SandTexture = m_renderer->GenPngTexture("Sand Particle.png");

	m_Tree = m_renderer->GenPngTexture("tunk.png");
	m_Chick = m_renderer->GenPngTexture("chicken.png");
	m_Duck = m_renderer->GenPngTexture("duck.png");
	m_Flower = m_renderer->GenPngTexture("flower.png");
	m_Turtle = m_renderer->GenPngTexture("turtle.png");

	m_FireTexture = m_renderer->GenPngTexture("firefield.png");

	m_SandParticle = m_renderer->CreateParticleObject(3000,
		-1000, 6000, 1000, 9000,
		5, 10, 5, 10, 
		-10, -10, -5, - 5);

	m_FireParticle = m_renderer->CreateParticleObject(1000,
		-1000, 0, 1000, 500,
		3, 5, 3, 5,
		0, 5, 0, 2);
	//Create Hero
	m_HeroID = AddObject(0, 0, 0, 1, 1, 0, 0, 0, 0, 20);
	m_objects[m_HeroID]->SetType(GSEObjectType::TYPE_HERO);
	m_objects[m_HeroID]->SetApplyPhysics(true);
	m_objects[m_HeroID]->SetLife(100000000.f);
	m_objects[m_HeroID]->SetLifeTime(100000000.f);
	m_objects[m_HeroID]->SetTextureID(m_HeroIdleTexture);
	m_objects[m_HeroID]->SetAnimNum(11);

	//wall
	int floor = AddObject(5.7, 50, 0, .5, 100, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_WallTexture);


	floor = AddObject(-5.7, 50, 0, .5, 100, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_WallTexture);

	//firefield
	floor = AddObject(0, -6, 0, 15, 10, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_FIRE);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_FireTexture);


	//brick
	for (int i = 0; i < GSE_FLOOR_NUM; ++i) 
	{

		floor = AddObject(0, i*5, 0, 20, .5, 0, 0, 0, 0, 10000);
		m_objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
		m_objects[floor]->SetApplyPhysics(true);
		m_objects[floor]->SetLife(100000000.f);
		m_objects[floor]->SetLifeTime(100000000.f);
		m_objects[floor]->SetTextureID(m_BrickTexture);
	}


	//animal
	floor = AddObject(2, 1, 0, 1, 1, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_ANIMAL);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_Tree);
	m_objects[floor]->SetAnimNum(11);
	m_objects[floor]->SetAnimalVel(1);

	floor = AddObject(4, 1, 0, 1, 1, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_ANIMAL);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_Duck);
	m_objects[floor]->SetAnimNum(10);
	m_objects[floor]->SetAnimalVel(3);

	floor = AddObject(-2, 1, 0, 1, 1, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_ANIMAL);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_Chick);
	m_objects[floor]->SetAnimNum(13);
	m_objects[floor]->SetAnimalVel(5);


	floor = AddObject(-3, 1, 0, 1, 1, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_ANIMAL);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_Flower);
	m_objects[floor]->SetAnimNum(11);
	m_objects[floor]->SetAnimalVel(0);


	floor = AddObject(3, 1, 0, 1, 1, 0, 0, 0, 0, 10000);
	m_objects[floor]->SetType(GSEObjectType::TYPE_ANIMAL);
	m_objects[floor]->SetApplyPhysics(true);
	m_objects[floor]->SetLife(100000000.f);
	m_objects[floor]->SetLifeTime(100000000.f);
	m_objects[floor]->SetTextureID(m_Turtle);
	m_objects[floor]->SetAnimNum(14);
	m_objects[floor]->SetAnimalVel(.5);





	m_BGM = m_Sound->CreateBGSound("BGM.mp3");
	m_Sound->PlayBGSound(m_BGM, true, 0.1f);
	m_SwordSound = m_Sound->CreateShortSound("swordSound.mp3");
}

GSEGame::~GSEGame() 
{
	
	//renderer delete
}
void GSEGame::Update(float elapsedTimeInSec, GSEInputs* inputs)
{
	//do garbage collecting
	DoGarbageCollect();


	GSEUpdateParams othersParam;
	GSEUpdateParams heroParam;

	memset(&othersParam, 0, sizeof(GSEUpdateParams));
	memset(&heroParam, 0, sizeof(GSEUpdateParams));
	//calc Force

	if (inputs->Key_SPACE) isStart = true;

	float forceAmount = 400.f;
	
	if (inputs->Key_W) {
		heroParam.forceY += 30*forceAmount;
		heroParam.upJump = true;
	}
	if (inputs->Key_A) {
		heroParam.forceX -= forceAmount;
	
	}
	if (inputs->Key_S) {
		heroParam.downJump = true;
	}
	if (inputs->Key_D) {
		heroParam.forceX += forceAmount;
	}
	
	if (inputs->Key_A&& !inputs->ARROW_LEFT && !inputs->ARROW_RIGHT)
	{
		
		m_objects[m_HeroID]->SetTextureID(m_HeroRunLeftTexture);
		m_objects[m_HeroID]->SetAnimNum(12);

	}
	if (inputs->Key_D && !inputs->ARROW_LEFT && !inputs->ARROW_RIGHT)
	{
		m_objects[m_HeroID]->SetTextureID(m_HeroRunTexture);
		m_objects[m_HeroID]->SetAnimNum(12);
	}

	if(!inputs->Key_A && !inputs->Key_D&&!inputs->ARROW_LEFT&&!inputs->ARROW_RIGHT)
	{
		m_objects[m_HeroID]->SetTextureID(m_HeroIdleTexture);
		m_objects[m_HeroID]->SetAnimNum(11);
	}

	//sword
	float swordPosX = 0.f;
	float swordPosY = 0.f;

	float testx, testy, testz;
	m_objects[m_HeroID]->GetPosition(&testx, &testy, &testz);

	if (inputs->ARROW_LEFT)
	{ 
		isgrab = true;
		swordPosX -= 1.f;	
		m_objects[m_HeroID]->SetTextureID(m_HeroAttackLeftTexture);
		m_objects[m_HeroID]->SetAnimNum(7);
	
	}
	if (inputs->ARROW_RIGHT)
	{
		isgrab = true;
		swordPosX += 1.f;
		m_objects[m_HeroID]->SetTextureID(m_HeroAttackTexture);
		m_objects[m_HeroID]->SetAnimNum(7);
	}

		
	if (!inputs->ARROW_LEFT&& !inputs->ARROW_RIGHT) { isgrab = false; }
	

	


	float swordDirSize = sqrtf(swordPosX * swordPosX + swordPosY * swordPosY);
	if (swordDirSize > 0.f)
	{
		float norDirX = swordPosX / swordDirSize;
		float norDirY = swordPosY / swordDirSize;

		float aX, aY, asX, asY;
		float bX, bY, bsX, bsY;
		float temp;

		m_objects[m_HeroID]->GetPosition(&aX, &aY, &temp);
		m_objects[m_HeroID]->GetSize(&asX, &asY);

		if (m_objects[m_HeroID]->GetRemainingCoolTime() < 0.f)
		{
			m_MagicID = AddObject(0.f, 0.f, 0.f, .7f, .7f, 0.f, 0.f, 0.f, 0.f, 1.f);
			m_objects[m_MagicID]->SetParentID(m_HeroID);
			m_objects[m_MagicID]->SetRelPosition(norDirX, norDirY, 0.f);
			m_objects[m_MagicID]->SetStickToParent(true);
			m_objects[m_MagicID]->SetLife(100.f);
			m_objects[m_MagicID]->SetLifeTime(0.3f);
			m_objects[m_HeroID]->ResetRemainingCoolTime();
			m_objects[m_MagicID]->SetTextureID(m_HeroMagicTextrue);
			m_objects[m_MagicID]->SetType(GSEObjectType::TYPE_MAGIC);
			m_Sound->PlayShortSound(m_SwordSound, false, 1.0f);

		}
	}

	//processing collison
	bool isCollide[GSE_MAX_OBJECTS];
	memset(isCollide, 0, sizeof(bool) * GSE_MAX_OBJECTS);
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		for (int j = i + 1; j < GSE_MAX_OBJECTS; j++) 
		{

			if (m_objects[i] != NULL && m_objects[j] != NULL)
			{
				bool collide = ProcessCollision(m_objects[i], m_objects[j]);
				if (collide)
				{
					isCollide[i] = true;
					isCollide[j] = true;
				}
			}
		}
	}

	
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_objects[i] != NULL)
		{
			if (!isCollide[i])
			{
				m_objects[i]->SetState(GSEObjectState::STATE_FALLING);

				if (i == m_HeroID)
				{
					m_objects[m_HeroID]->SetTextureID(m_HeroFallTextrue);
					m_objects[m_HeroID]->SetAnimNum(1);

				}
			}

			if (!isgrab&&m_objects[i]->GetState() == GSEObjectState::STATE_GRABED) 
			{
				m_objects[i]->SetState(GSEObjectState::STATE_FALLING);
				if (m_objects[i]->GetStickToParent()) {
					m_objects[i]->SetStickToParent(false);
				}

			}

		
		}

		
		
	}

	if (isStart && !isBadEnd && !isClear) {
		int clearCount = 0;

		//Update All Object
		for (int i = 0; i < GSE_MAX_OBJECTS; i++)
		{
			if (m_objects[i] != NULL)
			{
				if (m_objects[i]->GetOnRoofTop()) 
				{
					clearCount++;
				}

				if (i == m_HeroID)
				{
					m_objects[i]->Update(elapsedTimeInSec, &heroParam);
				}
				else
				{
					if (m_objects[i]->GetStickToParent())
					{
						float posX, posY, depth;
						float relPosX, relPosY, relDepth;
						int parentID = m_objects[i]->GetParentID();
						m_objects[parentID]->GetPosition(&posX, &posY, &depth);
						m_objects[i]->GetRelPosition(&relPosX, &relPosY, &relDepth);
						m_objects[i]->SetPosition(posX + relPosX, posY + relPosY, depth + relDepth);
						m_objects[i]->Update(elapsedTimeInSec, &othersParam);
					}
					else
					{
						m_objects[i]->Update(elapsedTimeInSec, &othersParam);
					}


				}

				if (clearCount >= 4)
				{
					isClear = true;
				}
			}
		}
	}

	//set Cam
	float x, y, z;
	m_objects[m_HeroID]->GetPosition(&x, &y, &z);
	m_renderer->SetCameraPos(x*10.f, (y+2)*100.f);
	
	if (cameraStartPosX == 0) 
	{
		cameraStartPosX = x * 10.f;
	}
	if (cameraStartPosY == 0)
	{
		cameraStartPosY = (y + 2) * 100.f;
	}

}


bool GSEGame::AABBCollison(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;

	GSEObjectType bType;


	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if (aMinX > bMaxX)
	{
		return false;
	}
	if (aMaxX < bMinX)
	{
		return false;
	}
	if (aMinY > bMaxY)
	{
		return false;
	}
	if (aMaxY < bMinY)
	{
		return false;
	}
	
	if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_ANIMAL)
	{
		return true;
	}
	else if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_MAGIC)
	{
		return true;
	}
	else if (a->GetState() == GSEObjectState::STATE_GRABED || b->GetState() == GSEObjectState::STATE_GRABED) 
	{
		
		return true;
	}
	/*else if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_FIRE || bType == GSEObjectType::TYPE_MOVABLE)
	{
		return true;
	}*/
	else 
	{
		AdjustPosition(a, b);
	}
	

	return true;
}

bool GSEGame::ProcessCollision(GSEObject* a, GSEObject* b)
{

	GSEObjectType aType, bType;
	a->GetType(&aType);
	b->GetType(&bType);

	bool isCollide = AABBCollison(a, b);
	if (isCollide)
	{
		//do something
		if (aType == GSEObjectType::TYPE_FIXED || bType == GSEObjectType::TYPE_FIXED)
		{
			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
		}

		if (aType == GSEObjectType::TYPE_FIRE && bType == GSEObjectType::TYPE_ANIMAL) 
		{
			b->SetIsDie(true);
			b->SetLife(0);
			b->SetLifeTime(0);

		}
		if (aType == GSEObjectType::TYPE_HERO && bType == GSEObjectType::TYPE_FIRE)
		{
			b->SetIsDie(true);
			isBadEnd = true;
		}
		if (aType == GSEObjectType::TYPE_MAGIC && bType == GSEObjectType::TYPE_ANIMAL)
		{
			float x, y, z;

			a->GetPosition(&x, &y, &z);
			b->SetState(GSEObjectState::STATE_GRABED);
			b->SetParentID(m_HeroID);
			b->SetRelPosition(x, y, 0.f);
			b->SetStickToParent(true);
		}
		if (bType == GSEObjectType::TYPE_MAGIC && aType == GSEObjectType::TYPE_ANIMAL)
		{
			if (isgrab&&grabCount<1) 
			{
				float x, y, z;
				float tx, ty, tz;

				b->GetPosition(&x, &y, &z);
				m_objects[m_HeroID]->GetPosition(&tx, &ty, &tz);
				a->SetParentID(m_HeroID);
				a->SetState(GSEObjectState::STATE_GRABED);
				a->SetRelPosition((x - tx), 0, 0.f);
				a->SetStickToParent(true);
				grabCount++;
			}
			if (!isgrab)
			{
				a->SetState(GSEObjectState::STATE_FALLING);
				a->SetStickToParent(false);
				if (grabCount > 0) { grabCount--; }
			}
		}
		
	}
	return isCollide;
}

void GSEGame::AdjustPosition(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;




	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);


	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if (aType == GSEObjectType::TYPE_MOVABLE || aType == GSEObjectType::TYPE_HERO || aType == GSEObjectType::TYPE_ANIMAL
		&& bType == GSEObjectType::TYPE_FIXED)
	{
		
		if (aMaxY > bMaxY)
		{
			aY = aY + (bMaxY - aMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);
		}
		else
		{
			if (aType != GSEObjectType::TYPE_HERO && aType != GSEObjectType::TYPE_ANIMAL)
			{
				aY = aY - (aMaxY - bMinY);

				a->SetPosition(aX, aY, 0.f);

				float vx, vy;
				a->GetVel(&vx, &vy);
				a->SetVel(vx, 0.f);
			}
		}

	}
	else if (bType == GSEObjectType::TYPE_MOVABLE || bType == GSEObjectType::TYPE_HERO || bType == GSEObjectType::TYPE_ANIMAL
		&& aType == GSEObjectType::TYPE_FIXED)
	{


		if (!(bMaxY > aMaxY&& bMinY < aMinY))
		{
			if (bMaxY > aMaxY)
			{
				bY = bY + (aMaxY - bMinY);

				b->SetPosition(bX, bY, 0.f);

				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(vx, 0.f);
			}
			else
			{
				if (bType != GSEObjectType::TYPE_HERO&&bType != GSEObjectType::TYPE_ANIMAL)
				{

					bY = bY - (bMaxY - aMinY);

					b->SetPosition(bX, bY, 0.f);

					float vx, vy;
					b->GetVel(&vx, &vy);
					b->SetVel(vx, 0.f);
				}
			}
		}

	}

	

		
}

void GSEGame::DoGarbageCollect()
{
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_objects[i] != NULL)
		{
			float life = m_objects[i]->GetLife();
			float lifeTime = m_objects[i]->GetLifeTime();
			if (life < 0.f || lifeTime < 0.f)
			{
				DeleteObject(i);
			}
		}
	}
}

float tempTime = 0.f;

void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// Renderer Test
	//m_renderer->DrawSolidRect(0, 0, 0, 4, 1, 0, 1, 1);

	//State BackGround
	if (!isStart) {
		m_renderer->DrawGround(0, 250, 0, 1000, 1100, 1, 1, 1, 1, 1, m_StartBGTexture);
	}
	if (isBadEnd) {
		m_renderer->SetCameraPos(cameraStartPosX, cameraStartPosY);

		m_renderer->DrawGround(0, 250, 0, 1000, 1100, 1, 1, 1, 1, 1, m_GameOverBGTextrue);
	}
	if (isClear) {
		m_renderer->DrawGround(0, 7250, 0, 1300, 1200, 1, 1, 1, 1, 1, m_ClearBGTexture);
	}

	if (isStart&&!isBadEnd&&!isClear) {
		//Draw background
		m_renderer->DrawGround(0, 500, 0, 2000, 2000, 1, 1, 1, 1, 1, m_BGTexture);
		m_renderer->DrawGround(0, 2000, 0, 2000, 1000, 1, 1, 1, 1, 1, m_BGTexture2);
		m_renderer->DrawGround(0, 3000, 0, 2000, 1000, 1, 1, 1, 1, 1, m_BGTexture2);
		m_renderer->DrawGround(0, 4000, 0, 2000, 1000, 1, 1, 1, 1, 1, m_BGTexture2);
		m_renderer->DrawGround(0, 5000, 0, 2000, 1000, 1, 1, 1, 1, 1, m_BGTexture2);
		m_renderer->DrawGround(0, 6000, 0, 2000, 1000, 1, 1, 1, 1, 1, m_BGTexture3);
		m_renderer->DrawGround(0, 7000, 0, 2000, 1000, 1, 1, 1, 1, 1, m_BGTexture3);
		m_renderer->DrawGround(0, 8000, 0, 2000, 1000, 1, 1, 1, 1, 1, m_BGTexture4);

		//Draw All object
		for (int i = 0; i < GSE_MAX_OBJECTS; i++)
		{
			if (m_objects[i] != NULL)
			{
				float x, y, depth;
				m_objects[i]->GetPosition(&x, &y, &depth);

				float sx, sy;
				m_objects[i]->GetSize(&sx, &sy);

				GSEObjectType temptype;
				m_objects[i]->GetType(&temptype);

				int temp = m_objects[i]->GetAnimIndex();
				int textureId = m_objects[i]->GetTextureID();

				x = x * 100.f;
				y = y * 100.f;
				sx = sx * 100.f;
				sy = sy * 100.f;

				if (textureId < 0)
				{
					m_renderer->DrawSolidRect(x, y, depth, sx, sy, 1, 0, 1, 1);
				}

				else if (temptype == GSEObjectType::TYPE_HERO || temptype == GSEObjectType::TYPE_ANIMAL) {

					m_renderer->DrawTextureRectAnim(
						x, y, depth,
						sx, sy, 1.f,
						1.f, 1.f, 1.f, 1.f,
						textureId,
						m_objects[i]->GetAnimNum(),
						1,
						temp,
						0);

				}
				else
				{
					m_renderer->DrawTextureRect(
						x, y, depth,
						sx, sy, 1.f,
						1.f, 1.f, 1.f, 1.f,
						textureId);
				}

				if (temptype == GSEObjectType::TYPE_FIRE) {
					m_renderer->DrawParticle(m_FireParticle,
						x, y, depth,
						1.f,
						1, 1, 1, 1,
						0, 10,
						m_FireTexture,
						1.f,
						tempTime,
						0.f);
				}
			}
		}


		m_renderer->DrawParticle(m_SandParticle,
			0, 0, 0,
			1.f,
			1, 1, 1, 1,
			0, 0,
			m_SandTexture,
			1.f,
			tempTime,
			0.f);

		

		tempTime += 0.016;
	}

	

}

int  GSEGame::AddObject(float x, float y, float depth,
	float sx, float sy,
	float velX, float velY,
	float accX, float accY,
	float Mass)
{
	int index = -1;
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_objects[i] == NULL)
		{
			index = i;
			break;
		}
	}
	if (index < 0) 
	{
		std::cout << "No empty object slot.." << std::endl;
		return -1;
	}

	m_objects[index] = new GSEObject();
	m_objects[index]->SetPosition(x, y, depth);
	m_objects[index]->SetSize(sx, sy);
	m_objects[index]->SetVel(velX, velY);
	m_objects[index]->SetAcc(accX, accY);
	m_objects[index]->SetMass(Mass);


	return index;
}

void GSEGame::DeleteObject(int index)
{
	
	if (m_objects[index] != NULL) 
	{
		delete m_objects[index];
		m_objects[index] = NULL;
	
	}
	else
	{
		std::cout << "Try to delete NULL object :" << index << std::endl;
	}
}


