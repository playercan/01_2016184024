#pragma once
#include "Renderer.h"
#include "GSEObject.h"
#include "GSEGlobal.h"
#include "Sound.h"
class GSEGame
{
public:
	GSEGame();
	~GSEGame();

	void RenderScene();
	int AddObject(float x, float y, float depth,
		float sx, float sy,
		float velX, float velY,
		float accX, float accY,
		float Mass
	);
	void DeleteObject(int index);
	void GSEGame::Update(float elapsedTimeInSec, GSEInputs* inputs);

private:
	bool AABBCollison(GSEObject* a, GSEObject* b);
	bool ProcessCollision(GSEObject* a, GSEObject* b);
	void AdjustPosition(GSEObject* a, GSEObject* b);
	void DoGarbageCollect();

	Renderer* m_renderer = NULL;
	Sound* m_Sound = NULL;
	GSEObject* m_objects[GSE_MAX_OBJECTS];
	int m_HeroID = -1;
	int m_MagicID = -1;

	bool isgrab;
	int grabCount = 0;
	
	//camera
	float cameraStartPosX = 0;
	float cameraStartPosY = 0;


	//state
	bool isStart = false;
	bool isBadEnd = false;
	bool isClear = false;


	int m_HeroIdleTexture = -1;
	int m_HeroRunTexture = -1;
	int m_HeroRunLeftTexture = -1;
	int m_HeroAttackTexture = -1;
	int m_HeroAttackLeftTexture = -1;
	int m_HeroFallTextrue = -1;
	

	
	int m_HeroMagicTextrue = -1;

	int m_BrickTexture = -1;
	int m_WallTexture = -1;

	int m_StartBGTexture = -1;
	int m_GameOverBGTextrue = -1;
	int m_ClearBGTexture = -1;

	int m_BGTexture = -1;
	int m_BGTexture2 = -1;
	int m_BGTexture3 = -1;
	int m_BGTexture4 = -1;


	int m_SandTexture = -1;
	int m_FireTexture = -1;

	int m_Tree = -1;
	int m_Chick = -1;
	int m_Duck = -1;
	int m_Flower = -1;
	int m_Turtle = -1;



	int m_BGM;
	int m_SwordSound;

	//particle
	int m_SandParticle = -1;
	int m_FireParticle = -1;
};

