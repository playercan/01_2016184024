#pragma once

#define GSE_MAX_OBJECTS 1000
#define GSE_GRAVITY 9.8f
#define GSE_WINDOWSIZE_X  1000
#define GSE_WINDOWSIZE_Y  1000
#define GSE_MAX_RANGE_X 4.82
#define GSE_FLOOR_NUM 15



typedef struct GSEInputs 
{
	bool ARROW_UP;
	bool ARROW_DOWN;
	bool ARROW_LEFT;
	bool ARROW_RIGHT;
	

	bool Key_W;
	bool Key_A;
	bool Key_S;
	bool Key_D;
	bool Key_SPACE;
};

typedef struct GSEUpdateParams
{
	float forceX;
	float forceY;
	bool downJump;
	bool upJump;
	bool grab;
};


enum GSEObjectType
{
	TYPE_HERO,
	TYPE_MOVABLE,
	TYPE_FIXED,
	TYPE_MAGIC,
	TYPE_FIRE,
	TYPE_ANIMAL
};

enum GSEObjectState
{
	STATE_FALLING,
	STATE_GROUND,
	STATE_DOWN_JUMP,
	STATE_GRABED
};