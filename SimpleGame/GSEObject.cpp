#include "stdafx.h"
#include "GSEObject.h"
#include <math.h>
GSEObject::GSEObject()
{
	m_PositionX = -1000000;
	m_PositionY = -1000000;
	m_Depth = -1000000;
	m_SizeX = -1000000;
	m_SizeY = -1000000;
	m_AccX = -1000000;
	m_AccY = -1000000;
	m_VelX = -1000000;
	m_VelY = -1000000;
	m_Mass = -1000000;

	m_Parent = -1;
	m_LifeTime = 0.f;
	m_Life = 0.f;
	m_ApplyPhysics = false;
	m_CoolTime = 0.3f;
	m_RemainingCoolTime = m_CoolTime;
	m_StickToParent = false;
	m_TextureID = -1;

	m_AnimIndex = 0;
	m_AnimNum = 0;
	m_AnimTimer = 0;

	m_animalTimer = 0;
	m_animalDir = 0;
	m_animalVel = 0;
	
	m_fireSpeed = 0;

	m_OnRoofTop = false;
	m_State = GSEObjectState::STATE_FALLING;
	m_Type = GSEObjectType::TYPE_MOVABLE;
}

GSEObject::~GSEObject()
{
}

void GSEObject::Update(float elapsedTimeInSec, GSEUpdateParams* param)
{
	m_RemainingCoolTime -= elapsedTimeInSec;
	m_LifeTime -= elapsedTimeInSec;

	if (m_Type != GSEObjectType::TYPE_ANIMAL && m_Type != GSEObjectType::TYPE_HERO&&m_Type != GSEObjectType::TYPE_FIRE)
		return;

	if (!m_ApplyPhysics)
		return;


	if (m_StickToParent) 
	{
		if (m_State == GSEObjectState::STATE_FALLING)
		{
			

			m_StickToParent = false;
			
		}
		return;
	}

	float t = elapsedTimeInSec;
	float tt = elapsedTimeInSec * elapsedTimeInSec;

	float accX = 0;
	float accY = 0;

	if (m_Type == GSEObjectType::TYPE_ANIMAL)
	{
		if (m_animalTimer > 60) 
		{
			m_animalTimer = 0;
			m_animalDir = (rand() % 3) - 1;
		}
		m_animalTimer++;
		m_VelX = m_animalDir*m_animalVel;
	}

	//Calc X axis & friction
	if (fabs(m_VelX) > 0.f )
	{
		//calce temporary

		accX = param->forceX / m_Mass;

		
		//sum with object'acc
		accX += m_AccX;

		//calc friction
		accX += m_VelX / fabs(m_VelX) * 0.7f * (-GSE_GRAVITY);

	
		//check wrong friction direction
		float tempVelX = m_VelX + accX * t;
		if (m_VelX * tempVelX < 0.f) 
		{
			m_VelX = 0.f;
		}
		else
		{
			m_VelX = tempVelX;
		}
		


	}
	else if (m_State == GSEObjectState::STATE_GROUND || m_State == GSEObjectState::STATE_FALLING)
	{
		//calce temporary
		accX = param->forceX / m_Mass;

		m_VelX = m_VelX + accX * t;
	}
	
	//Calc Y axis
	if (m_State == GSEObjectState::STATE_GROUND &&m_Type != GSEObjectType::TYPE_ANIMAL)
	{
		accY += m_AccY;

		//Jump!
		accY += param->forceY / m_Mass;

		if (param->downJump)
		{
			if (m_PositionY > 1)
			{
				m_PositionY -= 1;
			}
		}
		
	}
	else if ((m_State == GSEObjectState::STATE_FALLING|| m_State == GSEObjectState::STATE_GRABED|| m_State == GSEObjectState::STATE_DOWN_JUMP)
		&& m_Type != GSEObjectType::TYPE_FIRE)
	{
		
		accY -= GSE_GRAVITY;

	
		

	}

	m_VelY = m_VelY + accY * t;

	if (m_PositionX > GSE_MAX_RANGE_X)
	{

		m_VelX = 0;
		m_PositionX = GSE_MAX_RANGE_X;
		if (m_Type == GSEObjectType::TYPE_ANIMAL) 
		{
			m_animalDir = m_animalDir * -1;
		}
	}
	if (m_PositionX < -GSE_MAX_RANGE_X)
	{
		m_VelX = 0;

		m_PositionX = -GSE_MAX_RANGE_X;
		if (m_Type == GSEObjectType::TYPE_ANIMAL)
		{
			m_animalDir = m_animalDir * -1;
		}
	}

	if (m_VelY>10) 
	{
		m_VelY = 10;
	}
	if (m_VelY < -5) 
	{
		m_VelY = -5;
	}

	float deltaX = m_VelX * t + 0.5f * accX * tt;
	float deltaY = m_VelY * t + 0.5f * accY * tt;


	

	// update position
	m_PositionX = m_PositionX + deltaX;
	m_PositionY = m_PositionY + deltaY;




	if (m_Type == GSEObjectType::TYPE_FIRE) 
	{
		if (m_fireSpeed > 0.1) 
		{
			m_fireSpeed = 0.01;
		}
		else 
		{
			m_fireSpeed += 0.000001;

		}

		m_PositionY += m_fireSpeed;
	}

	if (m_AnimNum > 0) 
	{
		m_AnimTimer++;
		if (m_AnimTimer > 5)
		{
			m_AnimIndex++;
			m_AnimIndex = m_AnimIndex % m_AnimNum;
			m_AnimTimer = 0;
		}
	}

	if (m_PositionY > 70) 
	{
		m_OnRoofTop = true;
	}
	
	
	
}

void GSEObject::SetPosition(float x, float y, float depth)
{
	m_PositionX = x;
	m_PositionY = y;
	m_Depth = depth;
}

void GSEObject::SetRelPosition(float x, float y, float depth)
{
	m_RelPositionX = x;
	m_RelPositionY = y;
	m_RelDepth = depth;
}



void GSEObject::SetSize(float sx, float sy)
{
	m_SizeX = sx;
	m_SizeY = sy;
}

void GSEObject::SetVel(float x, float y)
{
	m_VelX = x;
	m_VelY = y;
}

void GSEObject::SetAcc(float x, float y)
{
	m_AccX = x;
	m_AccY = y;
}

void GSEObject::SetMass(float x)
{
	m_Mass = x;
}

void GSEObject::SetType(GSEObjectType type)
{
	m_Type = type;
}

void GSEObject::SetState(GSEObjectState state)
{
	m_State = state;
}

void GSEObject::SetParentID(int parentID)
{
	m_Parent = parentID;
}

void GSEObject::SetLifeTime(float lifeTime)
{
	m_LifeTime = lifeTime;
}

void GSEObject::SetLife(float life)
{
	m_Life = life;
}

void GSEObject::SetApplyPhysics(bool bPhy)
{
	m_ApplyPhysics = bPhy;
}

void GSEObject::SetCoolTime(float cooltime)
{
	m_CoolTime = cooltime;
}

void GSEObject::ResetRemainingCoolTime()
{
	m_RemainingCoolTime = m_CoolTime;
}

float GSEObject::GetCoolTime()
{
	return m_CoolTime;
}

float GSEObject::GetRemainingCoolTime()
{
	return m_RemainingCoolTime;
}

bool GSEObject::GetStickToParent()
{
	return m_StickToParent;
}

int GSEObject::GetAnimIndex()
{
	return m_AnimIndex;
}

int GSEObject::GetAnimNum()
{
	return m_AnimNum;
}

bool GSEObject::GetIsDie()
{
	return m_isDie;
}

bool GSEObject::GetOnRoofTop()
{
	return m_OnRoofTop;
}

void GSEObject::SetTextureID(int id)
{
	m_TextureID = id;
}

int GSEObject::GetTextureID()
{
	return m_TextureID;
}

void GSEObject::SetStickToParent(bool bStick)
{
	if (m_Parent < 0) 
	{
		std::cout << "This object has no parent" << std::endl;
		return;
	}
	if (m_StickToParent && !bStick)
	{
		m_PositionX = m_PositionX+m_RelPositionX;
		m_PositionY = m_PositionY+m_RelPositionY;
	}

	m_StickToParent = bStick;
}

void GSEObject::SetAnimIndex(int animIndex)
{
	m_AnimIndex = animIndex;
}

void GSEObject::SetAnimNum(int animNum)
{
	m_AnimNum = animNum;
}

void GSEObject::SetAnimalVel(float vel)
{
	m_animalVel = vel;
}

void GSEObject::SetIsDie(bool died)
{
	m_isDie = died;
}

void GSEObject::GetPosition(float* x, float* y, float* depth)
{
	*y = m_PositionY;
	*x = m_PositionX;
	*depth = m_Depth;
}

void GSEObject::GetRelPosition(float* x, float* y, float* depth)
{
	*x = m_RelPositionX;
	*y = m_RelPositionY;
	*depth = m_RelDepth;
}

void GSEObject::GetSize(float* sx, float* sy)
{
	*sx = m_SizeX;
	*sy = m_SizeY;
}

void GSEObject::GetVel(float* x, float* y)
{
	*x = m_VelX;
	*y = m_VelY;
}

void GSEObject::GetAcc(float* x, float* y)
{
	*x = m_AccX;
	*y = m_AccY;
}

void GSEObject::GetMass(float* x)
{
	*x = m_Mass;
}

void GSEObject::GetType(GSEObjectType* type)
{
	*type = m_Type;
}

GSEObjectState GSEObject::GetState()
{
	return m_State;
}

int GSEObject::GetParentID()
{
	return m_Parent;
}

float GSEObject::GetLifeTime()
{
	return m_LifeTime;
}

float GSEObject::GetLife()
{
	return m_Life;
}

bool GSEObject::GetApplyPhysics()
{
	return m_ApplyPhysics;
}


