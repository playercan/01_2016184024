#pragma once

#include "GSEGlobal.h"
#include <iostream>;

class GSEObject
{
public:
	GSEObject();
	~GSEObject();

	void Update(float elapsedTimeInSec, GSEUpdateParams* param);

	void SetPosition(float x, float y, float depth);
	void SetRelPosition(float x, float y, float depth);
	void SetSize(float sx, float sy);
	void SetVel(float x, float y);
	void SetAcc(float x, float y);
	void SetMass(float x);
	void SetType(GSEObjectType type);
	void SetState(GSEObjectState state);
	void SetParentID(int parentID);
	void SetLifeTime(float lifeTime);
	void SetLife(float life);
	void SetApplyPhysics(bool bPhy);
	void SetCoolTime(float cooltime);
	void ResetRemainingCoolTime();
	void SetStickToParent(bool bStick);
	void SetAnimIndex(int animIndex);
	void SetAnimNum(int animNum);
	void SetAnimalVel(float vel);
	void SetIsDie(bool died);


	void GetPosition(float* x, float* y, float* depth);
	void GetRelPosition(float* x, float* y, float* depth);
	void GetSize(float* sx, float* sy);
	void GetVel(float *x, float *y);
	void GetAcc(float *x, float *y);
	void GetMass(float* x);
	void GetType(GSEObjectType* type);
	GSEObjectState GetState();
	int GetParentID();
	float GetLifeTime();
	float GetLife();
	bool GetApplyPhysics();
	float GetCoolTime();
	float GetRemainingCoolTime();
	bool GetStickToParent();
	int GetAnimIndex();
	int GetAnimNum();
	bool GetIsDie();
	bool GetOnRoofTop();

	void SetTextureID(int id);
	int GetTextureID();
private:
	

	float m_PositionX, m_PositionY;
	float m_RelPositionX, m_RelPositionY;
	float m_Depth;
	float m_RelDepth;
	float m_SizeX, m_SizeY;
	float m_VelX, m_VelY;
	float m_AccX, m_AccY;
	float m_Mass;
	int m_Parent; //hero id, npc id...
	float m_LifeTime;
	float m_Life;
	bool m_ApplyPhysics;
	float m_CoolTime;
	float m_RemainingCoolTime;
	bool m_StickToParent;
	int m_TextureID;
	int m_AnimIndex;
	int m_AnimNum;
	int m_AnimTimer;

	int m_animalTimer;
	int m_animalDir;
	float m_animalVel;

	bool m_OnRoofTop;
	bool m_isDie;

	float m_fireSpeed;

	GSEObjectState m_State;
	GSEObjectType m_Type;
};

